const express = require('express')
const router = express.Router()
const Agency = require('../models/Agency')
// const User = require('../models/User')
const getAgencies = async function (req, res, next) {
  try {
    const agencys = await Agency.find({}).populate('agency_admin').exec()
    console.log(`agencys = ${agencys}`)
    res.status(200).json(agencys)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getAgency = async function (req, res, next) {
  const id = req.params.id
  try {
    const agency = await Agency.findById(id).exec()
    if (agency === null) {
      return res.status(404).json({
        message: 'Agency not found!!'
      })
    }
    res.json(agency)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addAgencys = async function (req, res, next) {
  const newAgency = new Agency({
    agency_name: req.body.agency_name,
    agency_users: req.body.agency_users,
    agency_description: req.body.agency_description,
    agency_admin: ''
  })
  try {
    await newAgency.save()
    res.status(201).json(newAgency)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateAgency = async function (req, res, next) {
  const agencyId = req.params.id
  try {
    const agency = await Agency.findById(agencyId)
    agency.agency_name = req.body.agency_name

    agency.agency_users = req.body.agency_users
    agency.agency_description = req.body.agency_description
    console.log(req.body.agency_admin)
    agency.agency_admin = req.body.agency_admin
    await agency.save()
    return res.status(200).json(agency)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteAgency = async function (req, res, next) {
  const agencyId = req.params.id
  try {
    // const users = await User.find().exec()
    // for (let i = 0; i < users.length; i++) {
    //   if (JSON.stringify(users[i].agency) === JSON.stringify(agencyId)) {
    //     users[i].agency = ''
    //     await users[i].save()
    //   }
    // }
    await Agency.findByIdAndDelete(agencyId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getAgencies) // GET All Users
router.get('/:id', getAgency) // Add new User
router.post('/', addAgencys) // GET User by Id
router.put('/:id', updateAgency) // Put update User
router.delete('/:id', deleteAgency) // Put update User
module.exports = router
