const express = require('express')
const Agency = require('../models/Agency')
const Approver = require('../models/Approver')
const BookingForm = require('../models/BookingForm')
const router = express.Router()
const User = require('../models/User')
const mongoose = require('mongoose')
const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).populate('agency').exec()
    console.log(`users = ${users}`)
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getUser = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.status(200).json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const getUserByAgency = async function (req, res, next) {
  const id = req.params.id
  try {
    const user = await User.find({ agency: id }).populate('agency').exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.status(200).json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const getUserByAgencyId = async function (req, res, next) {
  const id = req.params.id
  try {
    const userx = await User.findById(id).exec()
    const userAgency = []
    const user = await User.find({}).exec()
    for (let i = 0; i < user.length; i++) {
      if (JSON.stringify(user[i].agency) === JSON.stringify(userx.agency) && user[i].status.includes('ผู้พิจารณา')) {
        userAgency.push(user[i])
      }
    }
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(userAgency)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addUsers = async function (req, res, next) {
  const newUser = new User({
    firstname: req.body.firstname,
    lastname: req.body.lastname,
    username: req.body.username,
    password: req.body.password,
    status: req.body.status,
    agency: req.body.agency
  })
  try {
    await newUser.save()
    const agency = await Agency.findById(req.body.agency).exec()
    agency.agency_users.push(newUser)
    await agency.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId).exec()
    const oldAgency = await Agency.findById(user.agency).exec()
    const newAgency = await Agency.findById(req.body.agency).exec()
    user.firstname = req.body.firstname
    user.lastname = req.body.lastname
    user.username = req.body.username
    user.status = req.body.status
    if (user.agency !== req.body.agency) {
      oldAgency.agency_users.pop(user)
      user.agency = req.body.agency
      await user.save()
      newAgency.agency_users.push(user)
      oldAgency.save()
      newAgency.save()
    } else {
      user.agency = req.body.agency
      await user.save()
    }
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const updateAdminAgency = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    // user.firstname = req.body.firstname
    // user.lastname = req.body.lastname
    // user.username = req.body.username
    user.status.push('แอดมินหน่วยงาน')
    console.log(user)
    // user.agency = req.body.agency
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId).exec()
    const approver = await Approver.find().exec()
    const bookingForm = await BookingForm.find().exec()
    if (user.agency !== null) {
      const agency = await Agency.findById(user.agency).exec()
      agency.agency_users.pop(user)
      await agency.save()
    }
    for (let i = 0; i < bookingForm.length; i++) {
      if (JSON.stringify(bookingForm[i].booker) === JSON.stringify(userId)) {
        await BookingForm.findByIdAndDelete(bookingForm[i]._id)
      }
    }
    for (let i = 0; i < approver.length; i++) {
      console.log('12344444444', approver[i].approver_list.length)
      if (approver[i].approver_list.includes(mongoose.Types.ObjectId(userId))) {
        // approver[i].approver_list.splice(i, 1)
        const index = approver[i].approver_list.indexOf(mongoose.Types.ObjectId(userId))
        // approver[i].approver_list.pop()
        approver[i].approver_list.splice(index, 1)
        console.log('456789999999', mongoose.Types.ObjectId(userId))
        await approver[i].save()
      }
    }
    await User.findByIdAndDelete(userId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getUsers) // GET All Users
router.get('/:id', getUser) // Add new User
router.get('/a/:id', getUserByAgencyId) // Add new User
router.get('/b/:id', getUserByAgency) // Add new User
router.post('/', addUsers) // GET User by Id
router.put('/:id', updateUser) // Put update User
router.delete('/:id', deleteUser) // Put update User
router.patch('/:id', updateAdminAgency)
module.exports = router
