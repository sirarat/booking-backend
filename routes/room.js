const express = require('express')
const router = express.Router()
const Room = require('../models/Room')
const Building = require('../models/Building')
const getRooms = async function (req, res, next) {
  try {
    const Rooms = await Room.find({}).populate('building').exec()
    console.log(`Rooms = ${Rooms}`)
    res.status(200).json(Rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRoomsByAgency = async function (req, res, next) {
  const id = req.params.id // id agency
  try {
    const building = await Building.find({ agency_name: id }).exec()
    const Rooms = await Room.find({ building: building[0]._id }).populate('building').populate('approver').exec()
    console.log(`Rooms = ${Rooms}`)
    res.status(200).json(Rooms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getRoom = async function (req, res, next) {
  const id = req.params.id
  try {
    const room = await Room.findById(id).exec()
    if (room === null) {
      return res.status(404).json({
        message: 'Building not found!!'
      })
    }
    res.json(room)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addRoom = async function (req, res, next) {
  const newRoom = new Room({
    room_image: req.body.room_image,
    building: req.body.building,
    room_name: req.body.room_name,
    type_of_room: req.body.type_of_room,
    capacity: req.body.capacity,
    approver: req.body.approver
  })
  console.log('newRoom = ', newRoom)
  try {
    await newRoom.save()
    const building = await Building.findById(req.body.building)
    building.rooms.push(newRoom)
    building.save()
    res.status(201).json(newRoom)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    if (room.building !== req.body.building) {
      const oldBuilding = await Building.findById(room.building)
      oldBuilding.rooms.pop(room)
    }
    room.room_image = req.body.room_image
    room.building = req.body.building
    room.room_name = req.body.room_name
    room.type_of_room = req.body.type_of_room
    room.capacity = req.body.capacity
    room.approver = req.body.approver
    await room.save()
    const newBuilding = await Building.findById(req.body.building)
    newBuilding.rooms.push(room)
    return res.status(200).json(room)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteRoom = async function (req, res, next) {
  const roomId = req.params.id
  try {
    const room = await Room.findById(roomId)
    await Room.findByIdAndDelete(roomId)
    const uBuilding = await Building.findById(room.building)
    const listRoom = uBuilding.rooms
    for (let i = 0; i < listRoom.length; i++) {
      if (JSON.stringify(roomId) === JSON.stringify(listRoom[i])) {
        uBuilding.rooms.pop(room)
      }
    }
    await uBuilding.save()
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getRooms) // GET All Users
router.get('/:id', getRoom) // Add new User
router.get('/a/:id', getRoomsByAgency)
router.post('/', addRoom) // GET User by Id
router.put('/:id', updateRoom) // Put update User
router.delete('/:id', deleteRoom) // Put update User
module.exports = router
