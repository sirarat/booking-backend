const express = require('express')
const router = express.Router()
const Approver = require('../models/Approver')
const User = require('../models/User')
const getApprovers = async function (req, res, next) {
  try {
    const userId = req.params.id
    const agencyId = await User.findById(userId).exec()
    console.log(agencyId.agency)
    const approvers = await Approver.find({}).populate('approver_list', '-password').exec()
    const approverList = []
    for (let i = 0; i < approvers.length; i++) {
      const agency = approvers[i].agency
      console.log(agency)
      if (JSON.stringify(agency) === JSON.stringify(agencyId.agency)) {
        console.log('listId = ' + agency)
        console.log('xxxId = ' + agencyId.agency)
        approverList.push(approvers[i])
      }
    }
    res.status(200).json(approverList)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getApprover = async function (req, res, next) {
  const id = req.params.id
  try {
    const approver = await Approver.findById(id).exec()
    if (approver === null) {
      return res.status(404).json({
        message: 'Approver not found!!'
      })
    }
    res.json(approver)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addApprovers = async function (req, res, next) {
  const newApprover = new Approver({
    approver_name: req.body.approver_name,
    approver_list: req.body.approver_list,
    agency: req.body.agency
  })
  try {
    await newApprover.save()
    res.status(201).json(newApprover)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateApprover = async function (req, res, next) {
  const approverId = req.body._id
  console.log(approverId)
  console.log(req.body)
  try {
    const approver = await Approver.findById(approverId)
    approver.approver_name = req.body.approver_name
    approver.approver_list = req.body.approver_list
    approver.agency = req.body.agency
    await approver.save()
    return res.status(200).json(approver)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteApprover = async function (req, res, next) {
  const approverId = req.params.id
  try {
    await Approver.findByIdAndDelete(approverId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/a/:id', getApprovers) // GET All Users
router.get('/:id', getApprover) // Add new User
router.post('/', addApprovers) // GET User by Id
router.put('/:id', updateApprover) // Put update User
router.delete('/:id', deleteApprover) // Put update User
module.exports = router
