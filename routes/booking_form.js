const express = require('express')
const router = express.Router()
const BookingForm = require('../models/BookingForm')
const Approver = require('../models/Approver')
const getBookingForms = async function (req, res, next) {
  try {
    const bookingforms = await BookingForm.find({}).populate('booker', '-password').exec()
    console.log(`bookingforms = ${bookingforms}`)
    res.status(200).json(bookingforms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getBookingFormsByUserId = async function (req, res, next) {
  try {
    const userID = req.params.id
    const bookingforms = await BookingForm.find({ booker: userID }).populate({ path: 'room', populate: { path: 'building' } }).populate('booker').exec()
    console.log(`bookingforms = ${bookingforms}`)
    res.status(200).json(bookingforms)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}
const getBookingFormsByRoomID = async function (req, res, next) {
  const roomID = req.params.id
  try {
    const bookingform = await BookingForm.find({ room: roomID }).populate('booker', '-password').exec()
    const checkBookingForm = []
    for (let i = 0; i < bookingform.length; i++) {
      const approvers = JSON.parse(JSON.stringify(bookingform[i].approvers))
      let check = false
      for (let j = 0; j < approvers.length; j++) {
        if (approvers[j].booking_status === 'ไม่อนุมัติ' || approvers[j].booking_status === 'รอการอนุมัติ') {
          check = true
        }
      }
      if (check === false) {
        checkBookingForm.push(bookingform[i])
      }
    }
    if (bookingform === null) {
      return res.status(404).json({
        message: 'BookingForm not found!!'
      })
    }
    res.json(checkBookingForm)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
const getReservation = async function (req, res, next) {
  const approverID = req.params.id
  try {
    const approverList = []
    const bookingform = await BookingForm.find({}).populate({ path: 'room', populate: { path: 'building' } }).populate('booker').exec()
    console.log('length: ' + bookingform.length)
    for (let i = 0; i < bookingform.length; i++) {
      const approvers = JSON.parse(JSON.stringify(bookingform[i].approvers))
      for (let j = 0; j < approvers.length; j++) {
        if (((approvers[j].booking_status === 'รอการอนุมัติ') && (approvers[j].user !== approverID)) || (approvers[j].booking_status === 'ไม่อนุมัติ')) {
          console.log('not the right one: ' + approvers[j].user)
          break
        } else if ((approvers[j].booking_status === 'รอการอนุมัติ') && (approvers[j].user === approverID)) {
          approverList.push(bookingform[i])
          console.log('the right one: ' + approvers[j].user)
        }
      }
    }
    console.log(approverList)
    if (bookingform === null) {
      return res.status(404).json({
        message: 'bookingform not found!!'
      })
    }
    res.json(approverList)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}
const getBookingForm = async function (req, res, next) {
  const id = req.params.id
  try {
    const bookingform = await BookingForm.findById(id).populate({ path: 'room', populate: { path: 'building' } }).populate('booker').exec()
    if (bookingform === null) {
      return res.status(404).json({
        message: 'BookingForm not found!!'
      })
    }
    res.json(bookingform)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addBookingForms = async function (req, res, next) {
  console.log('id = ' + req.body.approvers)
  const approverEvent = await Approver.findById(req.body.approvers)
  const approverList = []
  for (let i = 0; i < approverEvent.approver_list.length; i++) {
    const json = {
      user: approverEvent.approver_list[i],
      approveDate: null,
      booking_status: 'รอการอนุมัติ'
    }
    approverList.push(json)
  }
  const newBookingForm = new BookingForm({
    room: req.body.room,
    booker: req.body.booker,
    start_time: req.body.start_time,
    end_time: req.body.end_time,
    approvers: approverList
  })
  try {
    await newBookingForm.save()
    res.status(201).json(newBookingForm)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBookingForm = async function (req, res, next) {
  const bookingformid = req.params.id
  try {
    const bookingform = await BookingForm.findById(bookingformid)
    const approverList = bookingform.approvers
    for (let i = 0; i < approverList.length; i++) {
      if (approverList[i].booking_status === 'รอการอนุมัติ') {
        approverList[i].booking_status = 'อนุมัติ'
        approverList[i].approveDate = Date.now()
        if (i === approverList.length - 1) {
          const allBookingForm = await BookingForm.find({}).exec()
          console.log(allBookingForm[0].approvers.length)
          for (let j = 0; j < allBookingForm.length; j++) {
            if ((JSON.stringify(bookingform._id) !== JSON.stringify(allBookingForm[j]._id)) && (JSON.stringify(bookingform.room) === JSON.stringify(allBookingForm[j].room)) && (allBookingForm[j].start_time <= bookingform.end_time) && (bookingform.start_time <= allBookingForm[j].end_time)) {
              console.log('---------------------------------------------')
              allBookingForm[j].approvers.forEach((item) => {
                console.log(item.booking_status)
                item.booking_status = 'ไม่อนุมัติ'
              })
              allBookingForm[j].save()
            }
          }
        }
        break
      }
    }
    bookingform.approvers = approverList
    await bookingform.save()
    return res.status(200).json(bookingform)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const updateCancelBookingForm = async function (req, res, next) {
  const bookingformid = req.params.id
  try {
    const bookingform = await BookingForm.findById(bookingformid)
    const approverList = bookingform.approvers
    for (let i = 0; i <= approverList.length; i++) {
      if (approverList[i].booking_status === 'รอการอนุมัติ') {
        approverList[i].booking_status = 'ไม่อนุมัติ'
        break
      }
    }
    bookingform.approvers = approverList
    await bookingform.save()
    return res.status(200).json(bookingform)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteBookingForm = async function (req, res, next) {
  const bookingformid = req.params.id
  try {
    await BookingForm.findByIdAndDelete(bookingformid)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBookingForms) // GET All Users
router.get('/user/:id', getBookingFormsByUserId)
router.get('/room/:id', getBookingFormsByRoomID) // GET All Users
router.get('/:id', getReservation)
router.get('/detail/:id', getBookingForm) // Add new User
router.post('/', addBookingForms) // GET User by Id
router.post('/confirm/:id', updateBookingForm) // Put update User
router.post('/cancel/:id', updateCancelBookingForm) // Put update User
router.delete('/:id', deleteBookingForm) // Put update User
module.exports = router
