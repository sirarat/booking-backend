const express = require('express')
const router = express.Router()
const Building = require('../models/Building')
const getBuildings = async function (req, res, next) {
  try {
    const buildings = await Building.find({}).populate('agency_name').exec()
    console.log(`buildings = ${buildings}`)
    res.status(200).json(buildings)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getBuilding = async function (req, res, next) {
  const id = req.params.id
  try {
    const building = await Building.findById(id).exec()
    if (building === null) {
      return res.status(404).json({
        message: 'Building not found!!'
      })
    }
    res.json(building)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addBuildings = async function (req, res, next) {
  const newBuilding = new Building({
    building_name: req.body.building_name,
    building_code: req.body.building_code,
    agency_name: req.body.agency_name,
    building_description: req.body.building_description,
    number_of_rooms: req.body.number_of_rooms,
    rooms: req.body.rooms,
    building_image: req.body.building_image,
    building_nameEng: req.body.building_nameEng
  })
  try {
    await newBuilding.save()
    res.status(201).json(newBuilding)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const updateBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    const building = await Building.findById(buildingId)
    building.building_name = req.body.building_name
    building.building_code = req.body.building_code
    building.agency_name = req.body.agency_name
    building.building_description = req.body.building_description
    building.number_of_rooms = req.body.number_of_rooms
    building.building_image = req.body.building_image
    building.building_nameEng = req.body.building_nameEng
    await building.save()
    return res.status(200).json(building)
  } catch (err) {
    return res.status(404).send({
      message: err.message
    })
  }
}

const deleteBuilding = async function (req, res, next) {
  const buildingId = req.params.id
  try {
    await Building.findByIdAndDelete(buildingId)
    return res.status(200).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
}

router.get('/', getBuildings) // GET All Users
router.get('/:id', getBuilding) // Add new User
router.post('/', addBuildings) // GET User by Id
router.patch('/:id', updateBuilding) // Put update User
router.delete('/:id', deleteBuilding) // Put update User
module.exports = router
