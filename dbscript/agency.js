const mongoose = require('mongoose')
const Agency = require('../models/Agency')
const User = require('../models/User')
mongoose.connect('mongodb://localhost:27017/booking_data')
async function clearAgency () {
  await Agency.deleteMany({})
}
async function main () {
  await clearAgency()
  const agency1 = new Agency({ agency_name: 'คณะวิทยาการสารสนเทศ', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะวิทยาการสารสนเทศ' })
  await agency1.save()
  const agency2 = new Agency({ agency_name: 'คณะวิศวกรรมศาสตร์', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะวิศวกรรมศาสตร์' })
  await agency2.save()
  const agency3 = new Agency({ agency_name: 'คณะแพทยศาสตร์', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะแพทยศาสตร์' })
  await agency3.save()
  const agency4 = new Agency({ agency_name: 'คณะสหเวชศาสตร์', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะสหเวชศาสตร์' })
  await agency4.save()
  const agency5 = new Agency({ agency_name: 'คณะวิทยาศาสตร์', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะวิทยาศาสตร์' })
  await agency5.save()
  const agency6 = new Agency({ agency_name: 'คณะศิลปกรรมศาสตร์', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะศิลปกรรมศาสตร์' })
  await agency6.save()
  const agency7 = new Agency({ agency_name: 'คณะศึกษาศาสตร์', agency_description: 'เป็นหน่วยงานที่ดูแลและจัดการเกี่ยวกับคณะศึกษาศาสตร์' })
  await agency7.save()
  const allUsers = await User.find()
  console.log(allUsers)
}
main().then(function () {
  console.log('finish')
})
