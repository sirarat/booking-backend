const mongoose = require('mongoose')
const BookingForm = require('../models/BookingForm')
const Room = require('../models/Room')
const User = require('../models/User')
const Approver = require('../models/Approver')
const { BOOKINGSTATUS } = require('../approve_constant')
mongoose.connect('mongodb://localhost:27017/booking_data')
async function clearBookingForm () {
  await BookingForm.deleteMany({})
}
async function main () {
  await clearBookingForm()
  const room4 = await Room.findOne({ room_name: 'IF-4C01' })
  const room5 = await Room.findOne({ room_name: 'IF-4C02' })
  const room6 = await Room.findOne({ room_name: 'IF-4C03' })
  const user6 = await User.findOne({ username: 'hrz.srr@gmail.com' })
  const user4 = await User.findOne({ username: 'mmodpoww@gmail.com' })
  const user7 = await User.findOne({ username: '_natruja_bb@gmail.com' })
  const approver1 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 1' })
  const approver2 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 2' })
  const approver3 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 3' })
  //   console.log(typeof (approver1.approver_list[0]))
  console.log(room4)

  const bookingform1 = new BookingForm({
    room: room4,
    booker: user6,
    start_time: new Date('2022-04-14 17:00'),
    end_time: new Date('2022-04-14 18:00'),
    // eslint-disable-next-line new-cap
    approvers: [{ user: approver1.approver_list[0], approveDate: Date.now(), booking_status: BOOKINGSTATUS.APPROVE },
      // eslint-disable-next-line new-cap
      { user: approver1.approver_list[1], approveDate: Date.now(), booking_status: BOOKINGSTATUS.APPROVE }]
  })
  const bookingform2 = new BookingForm({
    room: room5,
    booker: user4,
    start_time: new Date('2022-04-14 17:00'),
    end_time: new Date('2022-04-14 18:00'),
    // eslint-disable-next-line new-cap
    approvers: [{ user: approver2.approver_list[0], approveDate: Date.now(), booking_status: BOOKINGSTATUS.APPROVE },
      // eslint-disable-next-line new-cap
      { user: approver2.approver_list[1], approveDate: Date.now(), booking_status: BOOKINGSTATUS.APPROVE },
      { user: approver2.approver_list[2], approveDate: null, booking_status: BOOKINGSTATUS.WAITING }]
  })
  const bookingform3 = new BookingForm({
    room: room6,
    booker: user7,
    start_time: new Date('2022-04-15 17:00'),
    end_time: new Date('2022-04-15 18:00'),
    // eslint-disable-next-line new-cap
    approvers: [{ user: approver3.approver_list[0], approveDate: Date.now(), booking_status: BOOKINGSTATUS.APPROVE },
      // eslint-disable-next-line new-cap
      { user: approver3.approver_list[1], approveDate: null, booking_status: BOOKINGSTATUS.WAITING },
      { user: approver3.approver_list[2], approveDate: null, booking_status: BOOKINGSTATUS.WAITING }]
  })
  await bookingform1.save()
  await bookingform2.save()
  await bookingform3.save()
//   const allUsers = await Building.find()
//   console.log(allUsers)
}
main().then(function () {
  console.log('finish')
})
