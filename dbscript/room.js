const mongoose = require('mongoose')
const Building = require('../models/Building')
const Room = require('../models/Room')
const { ROOMTYPE } = require('../room_constant')
const Approver = require('../models/Approver')
mongoose.connect('mongodb://localhost:27017/booking_data')
async function clearRoom () {
  await Room.deleteMany({})
}
async function main () {
  await clearRoom()
  const building1 = await Building.findOne({ building_name: 'อาคารวิทยาการสารสนเทศ' })
  // const building1 = await Building.findOne({ building_code: 'IF' })
  const building2 = await Building.findOne({ building_name: 'อาคารวิศวกรรมภาคโยธา' })
  const building3 = await Building.findOne({ building_name: 'อาคารสมเด็จพระเทพรัตนราชสุดา' })
  const building4 = await Building.findOne({ building_name: 'อาคารสิรินธร' })
  const building5 = await Building.findOne({ building_name: 'อาคารวิทยาศาสตร์การแพทย์' })
  const building6 = await Building.findOne({ building_name: 'อาคารศิลปกรรมศาสตร์' })
  const building7 = await Building.findOne({ building_name: 'อาคาร 60 พรรษา มหาราชินี 1' })
  const approver1 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 1' })
  const approver2 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 2' })
  const approver3 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 3' })
  const approver4 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 4' })
  const approver5 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 5' })
  const approver6 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 6' })
  const approver7 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 7' })
  const approver8 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 8' })
  const approver9 = await Approver.findOne({ approver_name: 'ผู้พิจารณาแบบที่ 9' })
  const room1 = new Room({ room_image: 'meetingIF.jpg', building: building1, room_name: 'IF-11M280', type_of_room: [ROOMTYPE.MEETINGROOM], capacity: '280', approver: approver1 })
  await room1.save()
  building1.rooms.push(room1)
  const room2 = new Room({ room_image: 'lecIF.jpg', building: building1, room_name: 'IF-3M210', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '210', approver: approver2 })
  await room2.save()
  building1.rooms.push(room2)
  const room3 = new Room({ room_image: 'lecIF.jpg', building: building1, room_name: 'IF-4M210', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '210', approver: approver3 })

  await room3.save()
  building1.rooms.push(room3)
  const room4 = new Room({ room_image: 'labIF.jpg', building: building1, room_name: 'IF-4C01', type_of_room: [ROOMTYPE.LAB], capacity: '60', approver: approver2 })

  await room4.save()
  building1.rooms.push(room4)
  const room5 = new Room({ room_image: 'labIF.jpg', building: building1, room_name: 'IF-4C02', type_of_room: [ROOMTYPE.LAB], capacity: '60', approver: approver2 })

  await room5.save()
  building1.rooms.push(room5)
  const room6 = new Room({ room_image: 'labIF.jpg', building: building1, room_name: 'IF-4C03', type_of_room: [ROOMTYPE.LAB], capacity: '60', approver: approver2 })

  await room6.save()
  building1.rooms.push(room6)
  const room7 = new Room({ room_image: 'labIF.jpg', building: building1, room_name: 'IF-4C04', type_of_room: [ROOMTYPE.LAB], capacity: '60', approver: approver3 })

  await room7.save()
  building1.rooms.push(room7)

  const room8 = new Room({ room_image: 'labIF.jpg', building: building1, room_name: 'IF-3C04', type_of_room: [ROOMTYPE.LAB], capacity: '60', approver: approver3 })
  await room8.save()
  building1.rooms.push(room8)

  const room9 = new Room({ room_image: 'lecCE.jpg', building: building2, room_name: 'CE-101', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '60', approver: approver4 })
  await room9.save()
  building2.rooms.push(room9)

  const room10 = new Room({ room_image: 'lecCE.jpg', building: building2, room_name: 'CE-102', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '60', approver: approver4 })
  await room10.save()
  building2.rooms.push(room10)

  const room11 = new Room({ room_image: 'meetingCE.jpg', building: building2, room_name: 'CE-104', type_of_room: [ROOMTYPE.MEETINGROOM], capacity: '15', approver: approver4 })
  await room11.save()
  building2.rooms.push(room11)

  const room12 = new Room({ room_image: 'labCE.jpg', building: building2, room_name: 'CE-Lab', type_of_room: [ROOMTYPE.LAB], capacity: '40', approver: approver4 })
  await room12.save()
  building2.rooms.push(room12)

  const room13 = new Room({ room_image: 'lecMD.jpg', building: building3, room_name: 'MD-215', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '60', approver: approver5 })
  await room13.save()
  building3.rooms.push(room13)

  const room14 = new Room({ room_image: 'labMD.jpg', building: building3, room_name: 'MD-603', type_of_room: [ROOMTYPE.LAB], capacity: '20', approver: approver5 })
  await room14.save()
  building3.rooms.push(room14)

  const room15 = new Room({ room_image: 'meetingMD.jpg', building: building3, room_name: 'MD-602', type_of_room: [ROOMTYPE.MEETINGROOM], capacity: '60', approver: approver5 })
  await room15.save()
  building3.rooms.push(room15)

  const room16 = new Room({ room_image: 'lecSD.jpg', building: building5, room_name: 'SD-107', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '30', approver: approver6 })
  await room16.save()
  building5.rooms.push(room16)

  const room17 = new Room({ room_image: 'labSD.jpg', building: building5, room_name: 'SD-201', type_of_room: [ROOMTYPE.LAB], capacity: '35', approver: approver6 })
  await room17.save()
  building5.rooms.push(room17)

  const room18 = new Room({ room_image: 'meetingSD.jpg', building: building5, room_name: 'SD-117a', type_of_room: [ROOMTYPE.MEETINGROOM], capacity: '60', approver: approver6 })
  await room18.save()
  building5.rooms.push(room18)

  const room19 = new Room({ room_image: 'lecMS.jpg', building: building4, room_name: 'MS-103', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '60', approver: approver7 })
  await room19.save()
  building4.rooms.push(room19)

  const room20 = new Room({ room_image: 'labMS.jpg', building: building4, room_name: 'MS-406', type_of_room: [ROOMTYPE.LAB], capacity: '30', approver: approver7 })
  await room20.save()
  building4.rooms.push(room20)

  const room21 = new Room({ room_image: 'meetingMS.jpg', building: building4, room_name: 'MS-205', type_of_room: [ROOMTYPE.MEETINGROOM], capacity: '15', approver: approver7 })
  await room21.save()
  building4.rooms.push(room21)

  const room22 = new Room({ room_image: 'lecAB.jpg', building: building6, room_name: 'AB-104', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '60', approver: approver8 })
  await room22.save()
  building6.rooms.push(room22)

  const room23 = new Room({ room_image: 'labAB.jpg', building: building6, room_name: 'AB-201', type_of_room: [ROOMTYPE.LAB], capacity: '20', approver: approver8 })
  await room23.save()
  building6.rooms.push(room23)

  const room24 = new Room({ room_image: 'meetingAB.jpg', building: building6, room_name: 'AB-312', type_of_room: [ROOMTYPE.MEETINGROOM], capacity: '60', approver: approver8 })
  await room24.save()
  building6.rooms.push(room24)

  const room25 = new Room({ room_image: 'lecQS1.jpg', building: building7, room_name: 'QS1-1001', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '308', approver: approver9 })
  await room25.save()
  building7.rooms.push(room25)

  const room26 = new Room({ room_image: 'lecQS1.jpg', building: building7, room_name: 'QS1-103', type_of_room: [ROOMTYPE.LECTUREROOM], capacity: '40', approver: approver9 })
  await room26.save()
  building7.rooms.push(room26)

  await building1.save()
  await building2.save()
  await building3.save()
  await building4.save()
  await building5.save()
  await building6.save()
  await building7.save()
}
main().then(function () {
  console.log('finish')
})
