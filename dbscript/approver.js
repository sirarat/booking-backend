const mongoose = require('mongoose')
const Approver = require('../models/Approver')
const User = require('../models/User')
const Agency = require('../models/Agency')
mongoose.connect('mongodb://localhost:27017/booking_data')
async function clearApprover () {
  await Approver.deleteMany({})
}
async function main () {
  await clearApprover()
  const agency1 = await Agency.findOne({ agency_name: 'คณะวิทยาการสารสนเทศ' })
  const agency2 = await Agency.findOne({ agency_name: 'คณะวิศวกรรมศาสตร์' })
  const agency3 = await Agency.findOne({ agency_name: 'คณะแพทยศาสตร์' })
  const agency4 = await Agency.findOne({ agency_name: 'คณะสหเวชศาสตร์' })
  const agency5 = await Agency.findOne({ agency_name: 'คณะวิทยาศาสตร์' })
  const agency6 = await Agency.findOne({ agency_name: 'คณะศิลปกรรมศาสตร์' })
  const agency7 = await Agency.findOne({ agency_name: 'คณะศึกษาศาสตร์' })
  const user8 = await User.findOne({
    username: 'werapan@gmail.com'
  })
  const user9 = await User.findOne({
    username: 'jakkaman@gmail.com'
  })
  const user10 = await User.findOne({
    username: 'ureerat@gmail.com'
  })
  const user11 = await User.findOne({
    username: 'kisana@gmail.com'
  })
  const user23 = await User.findOne({
    username: 'tee.11@gmail.com'
  })
  const user24 = await User.findOne({
    username: 'kamol.123@gmail.com'
  })
  const user25 = await User.findOne({
    username: 'atity@gmail.com'
  })
  const user27 = await User.findOne({
    username: 'satit@gmail.com'
  })
  const user28 = await User.findOne({
    username: 'tida@gmail.com'
  })
  const user29 = await User.findOne({
    username: 'nanya@gmail.com'
  })
  const user31 = await User.findOne({
    username: 'irada@gmail.com '
  })
  const user32 = await User.findOne({
    username: 'nokja@gmail.com '
  })
  const user33 = await User.findOne({
    username: 'napa@gmail.com'
  })
  const user35 = await User.findOne({
    username: 'hahaha@gmail.com'
  })
  const user36 = await User.findOne({
    username: 'hok@gmail.com'
  })
  const user37 = await User.findOne({
    username: 'suvaida@gmail.com'
  })
  const user39 = await User.findOne({
    username: 'tidadadad@gmail.com'
  })
  const user40 = await User.findOne({
    username: 'sumrit@gmail.com'
  })
  const user41 = await User.findOne({
    username: 'sudaka@gmail.com'
  })
  const user43 = await User.findOne({
    username: 'adisor@gmail.com'
  })
  const user44 = await User.findOne({
    username: 'metida@gmail.com'
  })
  const user45 = await User.findOne({
    username: 'pornapa@gmail.com'
  })

  const approver1 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 1',
    agency: agency1
  })
  approver1.approver_list.push(user10)
  approver1.approver_list.push(user9)
  approver1.approver_list.push(user8)

  const approver2 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 2',
    agency: agency1
  })
  approver2.approver_list.push(user8)
  approver2.approver_list.push(user9)
  approver2.approver_list.push(user10)

  const approver3 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 3',
    agency: agency1
  })
  approver3.approver_list.push(user8)
  approver3.approver_list.push(user9)
  approver3.approver_list.push(user11)

  const approver4 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 4',
    agency: agency2
  })
  approver4.approver_list.push(user23)
  approver4.approver_list.push(user24)
  approver4.approver_list.push(user25)

  const approver5 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 5',
    agency: agency3
  })
  approver5.approver_list.push(user27)
  approver5.approver_list.push(user28)
  approver5.approver_list.push(user29)

  const approver6 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 6',
    agency: agency4
  })
  approver6.approver_list.push(user31)
  approver6.approver_list.push(user32)
  approver6.approver_list.push(user33)

  const approver7 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 7',
    agency: agency5
  })
  approver7.approver_list.push(user35)
  approver7.approver_list.push(user36)
  approver7.approver_list.push(user37)

  const approver8 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 8',
    agency: agency6
  })
  approver8.approver_list.push(user39)
  approver8.approver_list.push(user40)
  approver8.approver_list.push(user41)

  const approver9 = new Approver({
    approver_name: 'ผู้พิจารณาแบบที่ 9',
    agency: agency7
  })
  approver9.approver_list.push(user43)
  approver9.approver_list.push(user44)
  approver9.approver_list.push(user45)

  await approver1.save()
  await approver2.save()
  await approver3.save()
  await approver4.save()
  await approver5.save()
  await approver6.save()
  await approver7.save()
  await approver8.save()
  await approver9.save()
}
main().then(function () {
  console.log('Sed leaw ja')
})
