const BOOKINGSTATUS = {
  WAITING: 'รอการอนุมัติ',
  DISAPPROVE: 'ไม่อนุมัติ',
  APPROVE: 'อนุมัติ'
}
module.exports = {
  BOOKINGSTATUS
}
