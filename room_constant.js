const ROOMTYPE = {
  LECTUREROOM: 'ห้องเรียน',
  LAB: 'ห้องปฏิบัติการ',
  MEETINGROOM: 'ห้องประชุม'
}
module.exports = {
  ROOMTYPE
}
