const mongoose = require('mongoose')
const { Schema } = mongoose
const approverSchema = Schema({
  approver_name: String,
  approver_list: [{ type: Schema.Types.ObjectId, ref: 'User', default: [] }],
  agency: { type: Schema.Types.ObjectId, ref: 'Agency' }
})
module.exports = mongoose.model('Approver', approverSchema)
