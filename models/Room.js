const mongoose = require('mongoose')
const { Schema } = mongoose
const { ROOMTYPE } = require('../room_constant.js')
const roomSchema = Schema({
  room_image: String,
  building: { type: Schema.Types.ObjectId, ref: 'Building' },
  room_name: String,
  type_of_room: {
    type: [String],
    default: [ROOMTYPE.LECTUREROOM]
  },
  capacity: Number,
  approver: { type: Schema.Types.ObjectId, ref: 'Approver' }
})
module.exports = mongoose.model('Room', roomSchema)
