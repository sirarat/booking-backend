const mongoose = require('mongoose')
const { BOOKINGSTATUS } = require('../approve_constant')
const { Schema } = mongoose
const bookingformSchema = Schema({
  room: { type: Schema.Types.ObjectId, ref: 'Room' },
  booker: { type: Schema.Types.ObjectId, ref: 'User' },
  start_time: Date,
  end_time: Date,
  approvers: [{
    user: {
      type: Schema.Types.ObjectId,
      ref: 'User'
    },
    approveDate: Date,
    booking_status: {
      type: String,
      default: BOOKINGSTATUS.WAITING
    }
  }],
  request_datetime: Date
})
module.exports = mongoose.model('BookingForm', bookingformSchema)
