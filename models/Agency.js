const mongoose = require('mongoose')
const { Schema } = mongoose
const agencySchema = Schema({
  agency_name: String,
  agency_users: [{ type: Schema.Types.ObjectId, ref: 'User', default: [] }],
  agency_description: String,
  agency_admin: String
})
module.exports = mongoose.model('Agency', agencySchema)
