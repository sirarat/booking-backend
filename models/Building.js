const mongoose = require('mongoose')
const { Schema } = mongoose
const buildingSchema = Schema({
  building_name: String,
  building_code: String,
  agency_name: { type: Schema.Types.ObjectId, ref: 'Agency' },
  building_description: String,
  number_of_rooms: Number,
  rooms: [{ type: Schema.Types.ObjectId, ref: 'Room', default: [] }],
  building_image: String,
  building_nameEng: String
})
module.exports = mongoose.model('Building', buildingSchema)
