const { ROLE } = require('../constant.js')
const mongoose = require('mongoose')
const { Schema } = mongoose
const bcrypt = require('bcryptjs')
const userSchema = Schema({
  firstname: String,
  lastname: String,
  username: String,
  password: String,
  status: {
    type: [String],
    default: [ROLE.STUDENT]
  },
  agency: { type: Schema.Types.ObjectId, ref: 'Agency' }
})

userSchema.pre('save', function (next) {
  const user = this

  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (saltError, salt) {
      if (saltError) {
        return next(saltError)
      } else {
        bcrypt.hash(user.password, salt, function (hashError, hash) {
          if (hashError) {
            return next(hashError)
          }

          user.password = hash
          next()
        })
      }
    })
  } else {
    return next()
  }
})

module.exports = mongoose.model('User', userSchema)
