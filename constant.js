const ROLE = {
  STUDENT: 'นิสิต',
  DEAN: 'คณบดี',
  PROFESSOR: 'อาจารย์',
  SYSTEM_ADMIN: 'แอดมินระบบ',
  AGENCY_ADMIN: 'แอดมินหน่วยงาน',
  APPROVER: 'ผู้พิจารณา'
}
module.exports = {
  ROLE
}
